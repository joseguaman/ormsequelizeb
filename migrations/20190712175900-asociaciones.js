'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.addColumn(
        'cuenta',
        'id_persona',
        {
            type: Sequelize.INTEGER,
            references: {
                model: 'persona',
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'SET NULL'
        }
      )
      .then(() => {
          //return queryInterface.addColumn .....
      }
       )
       ;
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
   
   return queryInterface.removeColumn(
           'cuenta',
           'id_persona'
    )
    .then(() => {
          //return queryInterface.removeColumn .....
      }
    )
      ;   
  }
};
