'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('persona', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      cedula: {
        type: Sequelize.STRING(10)
      },
      apellidos: {
        type: Sequelize.STRING(30)
      },
      nombres: {
        type: Sequelize.STRING(30)
      },
      edad: {
        type: Sequelize.INTEGER
      },
      nro_reg: {
        type: Sequelize.STRING(20)
      },
      especialidad: {
        type: Sequelize.STRING(40)
      },
      fecha_nac: {
        type: Sequelize.DATEONLY
      },
      external_id: {
        type: Sequelize.UUID
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('persona');
  }
};