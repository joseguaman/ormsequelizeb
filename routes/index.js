var express = require('express');
var router = express.Router();
var passport = require('passport');
var persona = require('../controllers/PersonaControl');
var personaC = new persona();
var cuenta = require('../controllers/CuentaControl');
var cuentaC = new cuenta();
var auth = function (req, res, next) {
    if(req.isAuthenticated()) {
        next();
    } else {
        req.flash('error', "Debe iniciar sesion primero");
        res.redirect("/");
    }
}
/* GET home page. */
router.get('/', function (req, res, next) {    
    if (req.isAuthenticated()) {                    
        res.render('index', {title: "Principal",            
            andrea: 'fragmentos/principal', 
            sesion: true,
            usuario: req.user.nombre});
    } else {
        res.render('index', {title: 'Medicos', 
            msg: {error: req.flash('error'),
                ok: req.flash('success')}});
    }
});

router.post('/inicio_sesion',
        passport.authenticate('local-signin',
                {successRedirect: '/',
                    failureRedirect: '/',
                    failureFlash: true}
        ));
router.get('/cerrar_sesion', auth, cuentaC.cerrar_sesion);
router.get('/registro', function(req, res, next) {        
  res.render('index', { title: 'Registrate', sesion: true, andrea:"fragmentos/registro_medico" });
});
router.post('/registro', personaC.guardar);


module.exports = router;
