'use strict';
var models = require('./../models/');
var uuid = require('uuid');
class PersonaControl {
    guardar(req, res) {
        var persona = models.persona;
        var datos = {
            cedula: req.body.cedula,
            apellidos: req.body.apellidos,
            nombres: req.body.nombres,
            fecha_nac: req.body.fecha_nac,
            edad: req.body.edad,
            nro_reg: req.body.nro,
            external_id: uuid.v4(),
            especialidad: req.body.especialidad,
            cuenta: {
                correo: req.body.correo,
                clave: req.body.clave
                
            }
        };        
        persona.create(datos, {include: [{model: models.cuenta, as: 'cuenta'}]}).then(function (newYanela) {            
                req.flash('info', "Se ha registrado correctamente");
                res.redirect("/");
            
        });
        
    }
}
module.exports = PersonaControl;

