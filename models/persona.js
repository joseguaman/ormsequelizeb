'use strict';
module.exports = (sequelize, DataTypes) => {
  const Persona = sequelize.define('persona', {
    cedula: DataTypes.STRING,
    apellidos: DataTypes.STRING,
    nombres: DataTypes.STRING,
    edad: DataTypes.INTEGER,
    fecha_nac: DataTypes.DATEONLY,
    external_id: DataTypes.UUID,
    nro_reg: DataTypes.STRING,
    especialidad: DataTypes.STRING
  }, {freezeTableName: true});
  Persona.associate = function(models) {
    // associations can be defined here
    Persona.hasOne(models.cuenta, {foreignKey: 'id_persona', as: 'cuenta'});
  };
  return Persona;
};