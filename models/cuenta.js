'use strict';
module.exports = (sequelize, DataTypes) => {
  const cuenta = sequelize.define('cuenta', {
    correo: DataTypes.STRING,
    clave: DataTypes.STRING,
    estado: DataTypes.BOOLEAN
  }, {});
  cuenta.associate = function(models) {    
    cuenta.belongsTo(models.persona, {foreignKey: 'id_persona'});
  };
  return cuenta;
};